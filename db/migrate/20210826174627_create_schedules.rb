class CreateSchedules < ActiveRecord::Migration[5.2]
  def change
    create_table :schedules do |t|
      t.datetime :start
      t.datetime :stop
      t.float :hours_per_day

      t.timestamps
    end
  end
end
