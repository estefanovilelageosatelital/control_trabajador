class AddDescriptionToArchives < ActiveRecord::Migration[5.2]
  def change
    add_column :archives, :description, :text
    add_column :archives, :name, :string
  end
end
