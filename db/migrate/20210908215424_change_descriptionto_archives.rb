class ChangeDescriptiontoArchives < ActiveRecord::Migration[5.2]
  def change
    change_column :archives, :description, :string
  end
end
