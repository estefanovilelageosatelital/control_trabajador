# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create(
    email: 'evilelazarate@gmail.com',
    password: '123456',
    password_confirmation: '123456'
)

Admin::Employee.create(first_name: 'Renato', last_name: 'Dávila')
Admin::Employee.create(first_name: 'Sergio', last_name: 'Rivera')
Admin::Employee.create(first_name: 'Peter',  last_name: 'Cajusol')

Schedule.create(start: '2021-08-23 08:00:00', stop: '2021-08-23 18:00:00', hours_per_day: 10.0, employee_id: 1)
Schedule.create(start: '2021-08-25 08:30:00', stop: '2021-08-25 18:00:00', hours_per_day: 9.5,  employee_id: 1)
Schedule.create(start: '2021-08-26 08:00:00', stop: '2021-08-26 16:14:00', hours_per_day: 8.23, employee_id: 2)
Schedule.create(start: '2021-09-06 08:30:00', stop: '2021-09-06 18:30:00', hours_per_day: 10.0, employee_id: 4)