Rails.application.routes.draw do
  root 'schedules#new'
  devise_for :users

  resources :schedules, only: [:show, :new, :create] 

  namespace :admin do
    resources :employees do
      collection do
        get :report
      end

      member do
        get :api
      end
    end

    resources :archives
    root to: 'employees#index'
  end
end