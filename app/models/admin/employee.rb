class Admin::Employee < ApplicationRecord
    has_many :schedules

    def self.get_joined_employee
        Admin::Employee
            .select("employees.id, first_name, last_name, date(start) day, time(start) start, time(stop) stop, sum(schedules.hours_per_day) hours_per_month, employee_id")
            .joins("LEFT JOIN schedules ON employees.id=schedules.employee_id")
            .group(:id)
            # .where("(schedules.start>'#{ti}' OR schedules.start is null) AND (schedules.stop<'#{tf}' or schedules.stop is null)")
    end

    def total_per_month
        t = Time.now
        ti = t.beginning_of_month
        tf = t.end_of_month

        if day
            if day > ti && day < tf
                ('<span class="badge badge-pill badge-soft-success font-size-12">'+hours_per_month.to_s+' Horas</span>').html_safe
            else
                '<span class="badge badge-pill badge-soft-success font-size-12">NO TIENE HORAS EN ESTE MES</span>'.html_safe
            end
        else
            '<span class="badge badge-pill badge-soft-danger font-size-12">NO TIENE HORAS</span>'.html_safe
        end
    end

    private
        def get_csv(employees, options={})
            CSV.generate(options) do |csv|
                csv << ['ID', 'NOMBRES', 'DÍA', 'ENTRADA', 'SALIDA', 'SUBTOTAL_DE_HORAS']
    
                total = 0
                employees.each do |employee|
                    if employee.day
                        csv << [employee.id, employee.first_name+" "+employee.last_name, employee.day, employee.start, employee.stop, employee.hours_per_month]
                        total += employee.hours_per_month
                    else
                        csv << [employee.id, employee.first_name+" "+employee.last_name, "----------", "----------", "----------", "NO TIENE HORAS"]
                    end
                end

                csv << ["TOTAL_HORAS: ", total]
            end
        end

        def get_html(employees)
            html_str = '<html><head><title>Reporte</title></head><body><div style="padding: 5vh;"><h2 style="text-align: center !important;">Reporte</h2><hr><br><div class="table-responsive">'
            html_str += '<table border="2" class="table" style="width: 100%; border-collapse: collapse;"><thead><tr><th width="10%">#ID</th><th width="20%">Nombres</th><th width="20%">Día</th><th width="15%">Entrada</th><th width="15%">Salida</th><th width="20%">Subtotal-Horas</th></tr></thead><tbody>'

            total = 0

            if employees.length != 0
                employees.each do |employee|
                    html_str += 
                        "<tr>
                            <td>##{ employee.id }</td>
                            <td>#{ employee.first_name } #{ employee.last_name }</td>"

                    if employee.day
                        html_str += "
                            <td>#{ employee.day }</td>
                            <td>#{ employee.start }</td>
                            <td>#{ employee.stop }</td>
                            <td>#{ e.hours_per_month.to_s }</td>"

                        total += employee.hours_per_month
                    else
                        html_str += '
                            <td>----------</td>
                            <td>----------</td>
                            <td>----------</td>
                            <td align="right">"NO TIENE HORAS"</td>'
                    end

                    html_str += '</tr>'
                end
            else
                html_str += '
                    <tr style="text-align: center !important;">
                        <td colspan="6">No hay datos que mostrar.</td>
                    </tr>'
            end

            html_str += '</tbody></table><br><p>Total de horas: '+total.to_s+'</p></div></div></body></html>'
        end
end