class Schedule < ApplicationRecord
    def self.create_(params)
        today = Time.zone.now.strftime("%Y-%m-%d")

        if today == params[:day]
            case params[:type].to_i
            when 1
                Schedule.save_work_start(params)
            when 0
                Schedule.save_work_stop(params)
            else
                { status: :danger, message: 'Tipo inválido.' }
            end
        else
            { status: :danger, message: 'La fecha no es la correcta.' }
        end
    end

    private
        def self.save_work_start(params)
            day = Date.parse(params[:day])
            b = day.beginning_of_day
            e = day.end_of_day
            full_date = Time.parse(params[:day] + ' ' + params[:time])

            schedule = Schedule.find_or_create_by(start: b..e, employee_id: params[:employee_id])

            if schedule.start
                { status: :danger, message: 'Ya hay un entrada con esta fecha.' }
            else
                schedule.start = params[:day] + ' ' + params[:time]

                if schedule.save
                    { status: :created, message: 'La entrada fue creada correctamente.' }
                else
                    { status: :unprocessable_entity, message: schedule.errors }
                end
            end
        end

        def self.save_work_stop(params)
            day = Date.parse(params[:day])
            b = day.beginning_of_day
            e = day.end_of_day
            full_date = Time.parse(params[:day] + ' ' + params[:time])

            schedule = Schedule.find_by(start: b..e, stop: nil, employee_id: params[:employee_id])

            if schedule
                schedule.stop = full_date
                schedule.hours_per_day = (full_date - schedule.start)/3600

                if schedule.save
                    { status: :ok, message: 'La salida fue guardada correctamente.' }
                else
                    { status: :unprocessable_entity, message: schedule.errors }
                end
            else
                { status: :danger, message: 'No existe una entrada a la cual darle salida.' }
            end
        end
end