class SchedulesController < ApplicationController
    before_action :set_schedule, only: %i[ show edit update destroy ]

    def show
    end

    def new
        @employees = Admin::Employee.all
        @employee  = Admin::Employee.new
    end

    def create
        schedule = Schedule.create_(schedule_params)

        respond_to do |format|
            if schedule[:status]
                format.html { redirect_to @schedule, notice: 'El horario fue creado correctamente.' }
                format.json { render :show, status: :created, location: @schedule }
            else
                format.html { render :new, status: :unprocessable_entity }
                format.json { render json: @schedule.errors, status: :unprocessable_entity }
            end
        end
    end

    private
        def set_schedule
            @schedule = Schedule.find(params[:id])
        end

        def schedule_params
            params.require(:schedule).permit(:day, :time, :type, :employee_id)
        end
end
