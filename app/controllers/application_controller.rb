class ApplicationController < ActionController::Base
    rescue_from Exception, with: :handle_exception
end