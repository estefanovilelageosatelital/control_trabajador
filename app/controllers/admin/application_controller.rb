class Admin::ApplicationController < ApplicationController
    layout 'dashboard'
    before_action :authenticate_user!
    rescue_from Exception, with: :handle_exception
end