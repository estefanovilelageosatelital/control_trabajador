class Admin::EmployeesController < Admin::ApplicationController
    before_action :set_admin_employee, only: %i[ show edit update destroy api]

    def index
        @admin_employees = Admin::Employee.get_joined_employee
    end

    def show
    end

    def new
        @admin_employee = Admin::Employee.new
    end

    def edit
    end

    def create
        @admin_employee = Admin::Employee.new(admin_employee_params)

        respond_to do |format|
            if @admin_employee.save
                format.html { redirect_to admin_employee_url(@admin_employee), notice: "Employee was successfully created." }
                format.json { render :show, status: :created, location: @admin_employee }
            else
                format.html { render :new, status: :unprocessable_entity }
                format.json { render json: @admin_employee.errors, status: :unprocessable_entity }
            end
        end
    end

    def update
        respond_to do |format|
            if @admin_employee.update(admin_employee_params)
                format.html { redirect_to admin_employee_url(@admin_employee), notice: "Employee was successfully updated." }
                format.json { render :show, status: :ok, location: @admin_employee }
            else
                format.html { render :edit, status: :unprocessable_entity }
                format.json { render json: @admin_employee.errors, status: :unprocessable_entity }
            end
        end
    end

    def api
        res = @admin_employee
                .schedules
                .select("id, '<b>Día laboral</b> <br><br> Entrada: ' || start || '<br><br>Salida:' || stop title, start, stop")

        render json: res
    end

    def destroy
        @admin_employee.destroy

        respond_to do |format|
            format.html { redirect_to admin_employees_url, notice: "Employee was successfully destroyed." }
            format.json { head :no_content }
        end
    end


    def report
        render plain: 'aquí está el reporte'
=begin
        todos       = report_params[:todos].to_i
        employee_id = report_params[:employee_id]
        date_month  = report_params[:date_month]

        if user_signed_in?
            t = Time.now.strftime('%d-%m-%Y');

            name = t+"_"
            description = "<b>fecha: </b> #{t}<br>"
            attack = request.format.symbol.to_s
            file = nil

            if todos == 1
                name += "todos_mes-#{date_month}_"
                description += "<b>empleado: </b> todos<br><b>#mes: </b> #{date_month}<br>"
                @employees = Employee
                            .select("employees.id, first_name, last_name, date(start) day, time(start) start, time(stop) stop, sum(schedules.hours_per_day) hours_per_month, employee_id")
                            .joins("LEFT JOIN schedules ON employees.id=schedules.employee_id")
                            .where("cast(strftime('%m', schedules.start) as integer)=#{date_month}")
                            .group(:id)
            else
                name += "empleado-#{employee_id}_mes-#{date_month}_"
                description += "<b>empleado: </b> #{employee_id}<br><b>#mes: </b> #{date_month}<br>"
                @employees = Employee
                            .select("employees.id, first_name, last_name, date(start) day, time(start) start, time(stop) stop, sum(schedules.hours_per_day) hours_per_month, employee_id")
                            .joins("LEFT JOIN schedules ON employees.id=schedules.employee_id")
                            .where("cast(strftime('%m', schedules.start) as integer)=#{date_month} AND schedules.employee_id=#{employee_id}")
                            .group(:id)
            end

            name = "#{name}.#{attack}"
            description += "<b>tipo: </b> #{attack}"

            archives = Archive.all
            archives.each do |archive|
                if archive.name == name
                    file = url_for(archive.file)
                    break
                end
            end

            unless file
                case attack
                    when 'html'
                        file = get_html(@employees) # html_str
                    when 'csv'
                        file = get_csv(@employees) # csv_str
                    when 'xls'
                        file = get_csv(@employees, col_sep: "\t") # xls_str
                end

                @archive = Archive.new(name: name, description: description)
                @archive.file.attach(io: StringIO.new(file), filename: name)
                @archive.save
            end

            respond_to do |format|
                if @archive
                    # SE HA CREADO UN NUEVO ARCHIVO
                    format.html { send_data file, filename: name }
                    format.csv  { send_data file, filename: name }
                    format.xls  { send_data file, filename: name }
                    # format.xls {send_data @employees.to_csv(col_sep: "\t") }E
                else
                    # SE REDIRECCIONARÁ A UN ARCHIVO YA CREADO
                    format.html { redirect_to file }
                    format.csv  { redirect_to file }
                    format.xls  { redirect_to file }
                end
            end
        else
            redirect_to root_path
        end
=end
    end

    private
        def set_admin_employee
            @admin_employee = Admin::Employee.find(params[:id])
        end

        def admin_employee_params
            params.require(:admin_employee).permit(:first_name, :last_name)
        end

        # def report_params
        #     params.permit(:todos, :employee_id, :date_month)
        # end
end