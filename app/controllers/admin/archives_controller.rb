class Admin::ArchivesController < Admin::ApplicationController
	before_action :set_admin_archive, only: %i[ show edit update destroy ]

    def index
		@admin_archives = Admin::Archive.all
    end

    def show
    end

    def new
		@admin_archive = Admin::Archive.new
    end

    def edit
    end

    def create
		@admin_archive = Admin::Archive.new(admin_archive_params)
		@admin_archive.file.attach(admin_archive_params[:file])

		respond_to do |format|
			if @admin_archive.save
				format.html { redirect_to admin_archive_url(@admin_archive), notice: "Archive was successfully created." }
				format.json { render :show, status: :created, location: @admin_archive }
			else
				format.html { render :new, status: :unprocessable_entity }
				format.json { render json: @admin_archive.errors, status: :unprocessable_entity }
			end
		end
    end

    def update
		@admin_archive.file.purge
		@admin_archive.file.attach(admin_archive_params[:file])

		respond_to do |format|
			if @admin_archive.update(admin_archive_params)
				format.html { redirect_to admin_archive_url(@admin_archive), notice: "Archive was successfully updated." }
				format.json { render :show, status: :ok, location: @admin_archive }
			else
				format.html { render :edit, status: :unprocessable_entity }
				format.json { render json: @admin_archive.errors, status: :unprocessable_entity }
			end
		end
    end

    def destroy
		@admin_archive.destroy
		@admin_archive.file.purge

		respond_to do |format|
			format.html { redirect_to admin_archive, notice: "Archive was successfully destroyed." }
			format.json { head :no_content }
		end
    end

    private
		def set_admin_archive
			@admin_archive = Admin::Archive.find(params[:id])
		end

		def admin_archive_params
			params.require(:admin_archive).permit(:name, :description, :file)
		end
end