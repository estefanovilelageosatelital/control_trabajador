module HandleException
	extend ActiveSupport::Concern

	TEMPLATE = 'errors/index.html.erb'

	# included do
	# 	before_action :set_route, only: %i[ edit_cancel cancel ]
	# 	before_action :change_route_status, only: :edit_cancel
	# 	before_action :check_update_permission, only: %i[ edit_cancel ]
	# end

	# def edit_cancel
	# 	@cancel_route_patch = cancel_route_patch
	# 	render 'panel/routes/edit_cancel'
	# end

	private
		def handle_exception(ex)
			@message = ex.to_s
			respond_to do |format|
				format.html { render template: TEMPLATE, status: :unprocessable_entity }
				format.json { render json: @message,     status: :unprocessable_entity }
			end
		end
=begin
		def set_route
			@route = current_customer.routes.find(params[:id])
		end

		def cancel_route_params
			params.require(:route).permit(:route_history_message).merge(route_history_user: current_user, status: :canceled)
		end
=end
end