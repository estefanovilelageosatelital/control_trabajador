(function () {
  this.App || (this.App = {})

  const $document = $(document)
  var objects = []
  var events  = []

  App.isPreviewPage = function () {
    return document.documentElement.hasAttribute('data-turbolinks-preview')
  }

  App.new = function (type, attributes = {}) {
    const Klass = App[type]

    if (App.isPreviewPage()) {
      return
    }

    let obj = new Klass(attributes)
    objects.push(obj)
    return obj
  }

  App.on = function () {
    if (App.isPreviewPage()) {
      return
    }

    let obj = arguments[0]

    if (obj == null || undefined) {
      throw 'invalid obj'
    }

    // if ((L && obj instanceof L.Map) || obj instanceof jQuery) {
    //   obj = obj
    // } else {
    //   obj = $(obj)
    // }

    let event = arguments[1]

    if (typeof event !== 'string') {
      throw 'invalid event'
    }

    let selector = arguments[2]
    let callback = arguments[3]

    if (typeof selector !== 'string' && !$.isFunction(selector)) {
      throw 'invalid selector o callback'
    }

    if ($.isFunction(selector)) {
      callback = selector
      selector = null
    }

    if (selector) {
      obj.on(event, selector, callback)
    } else {
      obj.on(event, callback)
    }

    events.push({
      obj: obj,
      event: event,
      selector: selector,
      callback: callback
    })
  }

  $.fn.renderAlert = function (message, title = null, opts = {}) {
    var type = opts.type || 'success'
    var mode = opts.mode || 'replace'

    if (type == 'ok') type = 'success'
    if (type == 'error') type = 'danger'

    let titleHtml = ''

    if (title) {
      titleHtml = `<h4 class="alert-heading">${title}</h4>`
    }

    var alertHtml = `
      <div class="alert alert-${type} alert-dismissible fade show" role="alert">
        ${titleHtml}
        <p class="mb-0">${message}</p>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    `

    switch (mode) {
      case 'prepend':
        $(this).prepend(alertHtml)
        break;
      case 'append':
        $(this).append(alertHtml)
        break;
      case 'replace':
        $(this).html(alertHtml)
        break;
      default:
        throw 'unknown mode'
    }
  }

  $.fn.renderErrorAlert = function (message, title = null, opts = {}) {
    this.renderAlert(message, title, { type: 'error', mode: opts.mode })
  }

  $.fn.renderSuccessAlert = function (message, title = null, opts = {}) {
    this.renderAlert(message, title, { mode: opts.mode })
  }

  $.fn.clearFormErrors = function () {
    const $form = $(this)
    const containerErrors = $form.data('notification')

    if (containerErrors === true) {
      $('.form-errors', $form).remove()
    } else {
      $(containerErrors).html('')
    }

    $('.form-group', $form).each(function () {
      $('.invalid-feedback', $form).html('')
      $('.valid-feedback', $form).html('')
      $('.form-control', $form).removeClass('is-invalid is-valid')
    })
  }

  $.fn.showFormErrors = function (data) {
    const $form = $(this)
    const model = $form.data('model') || ''
    const errors = data.errors || []
    const containerErrors = $form.data('notification')
    let htmlErrors = '', htmlAlert = ''

    $form.clearFormErrors()

    $.each(errors, function (field, messages) {
      const $input = $(`input[name="${model}[${field}]"]`)
      htmlErrors += messages.map(msg => `<li>${msg}</li>`)
      $input.addClass('is-invalid')
    })

    if (htmlErrors == '') return

    htmlAlert = `
      <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <ul class="mb-0">${htmlErrors}</ul>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    `

    if (containerErrors === true) {
      $form.prepend(`<div class="form-errors">${htmlAlert}</div>`)
    } else {
      $form.find(containerErrors).html(htmlAlert)
    }
  }

  $document.on('turbolinks:load', () => {
    App.on($document, 'turbolinks:before-cache', () => {
      $('#alerts').html('')

      events.forEach(event => {
        if (event.selector) {
          event.obj.off(event.event, event.selector, event.callback)
        } else {
          event.obj.off(event.event, event.callback)
        }
      })

      objects.forEach(obj => {
        if (typeof obj.onReset === 'function') {
          obj.onReset()
        }
      })

      events = []
      objects = []
    })

    App.on($document, 'ajax:success', '[data-notification]', (e) => {
      const [data, status, xhr] = e.detail
      const $target = $(e.currentTarget)
      const containerErrors = $target.data('notification') === true ? '#alerts' : $target.data('notification')

      if (data.notice) {
        $(containerErrors).renderSuccessAlert(data.notice)
      }
    })

    App.on($document, 'ajax:error', '[data-notification]', (event) => {
      const [data, status, xhr] = event.detail
      const $target = $(event.currentTarget)

      if ($target.prop('tagName') == 'FORM') {
        $target.showFormErrors(data)
        return
      }

      // if ($target.prop('tagName') == 'A' && data.alert) {
      //   $(containerErrors).renderErrorAlert(data.alert)
      //   return
      // }

      const containerErrors = $target.data('notification') === true ? '#alerts' : $target.data('notification')
      $(containerErrors).renderErrorAlert(data.alert)
    })
  })

}).call(this);
